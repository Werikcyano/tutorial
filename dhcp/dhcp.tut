#!/bin/bash

# TUTORIAL: DHCP
lines=(
"
SOBRE O TUTORIAL:\n
- Tutorial DHCP\n
- Autor: Marcelo Akira Inuzuka <akira@marceloakira.com>\n
- Licença: creative-commons by-sa (cc-by-sa)\n
- Para copiar, execute em um terminal:\n
$ git clone https://gitlab.com/marceloakira/tutorial.git'\n
- Para executar, digite em um terminal:\n
$ cd tutorial\n
$ ./tutorial dhcp\n
- Para prosseguir: tecle <ENTER> ou use as setas\n
- Para sair: tecle: <CTRL+C>\n
"

"
INSTRUÇÃO PRÉVIA:\n
- Este tutorial utiliza o Debian 7.3 como ambiente\n
- Alguns comandos necessitam permissão de super-usuário (root)\n
- São utizadas máquinas virtuais: \n
  - MV1: 'debian-7.3-adsi-2014-mv1.ova'\n
  - MV2: 'debian-minimo.ova' \n
  - usuário: root, senha: 123456\n
  - baixe em http://www.inf.ufg.br/~marceloakira/vms\n
- A MV1 está configurada com duas interfaces:\n
  - eth0 = ligado a rede local\n
  - eth1 = conectado a outra máquina virtual (MV2)\n
- A MV2 está configurada com duas interfaces:\n
  - eth0 = conectado a MV1 por rede interna\n
  - eth1 = também conectado a MV1 por rede interna\n
- Consulte este tutorial para configurar uma rede interna:\n
  - http://guiatech.net/virtualbox-criando-uma-rede-interna/
- O vídeo deste tutorial está gravado em \n
  - http://www.inf.ufg.br/~marceloakira/videos/tutorial-dhcp\n
"

"
INSTRUÇÃO 1: CONCEITOS BÁSICOS SOBRE DHCP\n
- O DHCP significa Protocolo de Configuração Dinâmica de Hospedeiro\n
- É um protocolo cliente/servidor sucessor do BOOTP\n
- O servidor ofere configuração de hospedeiro para seus clientes\n
- Configuração oferecida: número IP, nome do host, roteador padrão, etc... \n
"

"
PASSO 1: EXECUTAR O CLIENTE DHCP\n
- Vamos executar o cliente DHCP e analisar como ele funciona\n
- Digite o comando 'dhclient -v eth0' em um terminal como root\n
- O resultado do seu comando deve ser parecido com esse:\n
# dhclient -v eth0 \n
\n
Internet Systems Consortium DHCP Client 4.2.2 \n
\n
Copyright 2004-2011 Internet Systems Consortium.\n  
All rights reserved.\n
For info, please visit https://www.isc.org/software/dhcp/\n
Listening on LPF/eth0/08:00:27:69:ef:49\n
Sending on   LPF/eth0/08:00:27:69:ef:49\n
Sending on   Socket/fallback\n
DHCPREQUEST on eth0 to 255.255.255.255 port 67\n
DHCPREQUEST on eth0 to 255.255.255.255 port 67\n
\n
DHCPNAK from 192.168.0.1\n
\n
DHCPDISCOVER on eth0 to 255.255.255.255 port 67 interval 8\n
DHCPREQUEST on eth0 to 255.255.255.255 port 67\n
DHCPOFFER from 192.168.0.1\n
DHCPACK from 192.168.0.1\n
bound to 192.168.0.41 -- renewal in 13344201 seconds.\n
\n
"

"
PASSO 1 $RED(comentado)$END: EXECUTAR O CLIENTE DHCP\n
- Vamos executar o cliente DHCP e analisar como ele funciona\n
- Digite o comando 'dhclient -v eth0' em um terminal como root\n
- O resultado do seu comando deve ser parecido com esse $RED(veja os comentários em vermelho:)$END\n
# dhclient -v eth0 \n
$RED (a interface eth0 está conectado na rede local )$END\n
Internet Systems Consortium DHCP Client 4.2.2 \n
$RED (Cliente do ISC) $END \n
Copyright 2004-2011 Internet Systems Consortium.\n  
All rights reserved.\n
For info, please visit https://www.isc.org/software/dhcp/\n

Listening on LPF/eth0/08:00:27:69:ef:49\n
Sending on   LPF/eth0/08:00:27:69:ef:49\n
Sending on   Socket/fallback\n
DHCPREQUEST on eth0 to 255.255.255.255 port 67\n
DHCPREQUEST on eth0 to 255.255.255.255 port 67\n
$RED(foi enviado uma requisição em broadcast)$END\n
DHCPNAK from 192.168.0.1\n
$RED (foi recebido uma resposta do servidor) $END \n
DHCPDISCOVER on eth0 to 255.255.255.255 port 67 interval 8\n
DHCPREQUEST on eth0 to 255.255.255.255 port 67\n
DHCPOFFER from 192.168.0.1\n
DHCPACK from 192.168.0.1\n
bound to 192.168.0.41 -- renewal in 13344201 seconds.\n
$RED (foi obtido um número IP do servidor) $END \n
"
"
PASSO 2: VERIFICAR SE O SERVIDOR DHCP JÁ ESTÁ INSTALADO\n
- Vamos agora instalar um servidor DHCP\n
- Antes, verifique o que já tem instalado de DHCP\n
- Execute o comando no terminal:
# dpkg -l | grep dhcp\n
\n
\n
ii  isc-dhcp-client                      4.2.2.dfsg.1-5+deb70u6             i386         ISC DHCP client\n
ii  isc-dhcp-common                      4.2.2.dfsg.1-5+deb70u6             i386         common files used by all the isc-dhcp* packages\n
\n
"

"
PASSO 2$RED(comentado)$END: VERIFICAR SE O SERVIDOR DHCP JÁ ESTÁ INSTALADO\n
- Vamos agora instalar um servidor DHCP\n
- Antes, verifique o que já tem instalado de DHCP\n
- Execute o comando no terminal:
# dpkg -l | grep dhcp\n
$RED(o 'dpkg -l' lista todos os pacotes)\n$END
$RED(e o 'grep dhcp' filtra o linhas com 'dhcp')\n$END
ii  isc-dhcp-client                      4.2.2.dfsg.1-5+deb70u6             i386         ISC DHCP client\n
ii  isc-dhcp-common                      4.2.2.dfsg.1-5+deb70u6             i386         common files used by all the isc-dhcp* packages\n
$RED(está instalado um pacote do cliente DHCP e arquivos comuns)$END\n
"

"
PASSO 3: INSTALAR O SERVIDOR DHCP\n
- Vamos agora instalar o servidor DHCP\n
- Nem sempre dá para lembrar o nome do pacote\n
- Para pesquisar, use o comando 'aptitude search dhcp': \n
# aptitude search dhcp\n
\n
p  isc-dhcp-server                                                - ISC DHCP server for automatic IP address assignment\n
\n
...\n
"

"
PASSO 3$RED(comentado)$END: INSTALAR O SERVIDOR DHCP\n
- Vamos agora instalar o servidor DHCP\n
- Nem sempre dá para lembrar o nome do pacote\n
- Para pesquisar, use o comando 'aptitude search dhcp': \n
# aptitude search dhcp\n
... $RED (vários pacotes) $END\n
p  isc-dhcp-server                                                - ISC DHCP server for automatic IP address assignment\n
$RED(esse pacote é o que queremos - trata-se do servidor DHCP da ISC (Internet Software Consortium))$END\n
... $RED (vários outros pacotes) $END\n
"

"
PASSO 4: ATUALIZAR A LISTA DE PACOTES:\n
- Sempre atualize a lista antes de instalar um pacote\n
- o comando 'aptitude update' atualiza a lista de pacotes:\n
# aptitude update \n
\n
...\n
"

"
PASSO 4$RED(comentado)$END: ATUALIZAR A LISTA DE PACOTES:\n
- Sempre atualize a lista antes de instalar um pacote\n
- o comando 'aptitude update' atualiza a lista de pacotes:\n
# aptitude update \n
$RED(execute esse comando em terminal como root) $END\n
... $RED (verifique se a lista de pacotes foi atualizada) $END\n
"

"
PASSO 5: INSTALAR O SERVIDOR DHCP:\n
Agora podemos instalar o servidor através do comando:\n
# aptitude install isc-dhcp-server\n
Os NOVOS pacotes a seguir serão instalados:\n
  isc-dhcp-server \n
0 pacotes atualizados, 1 novos instalados, 0 a serem removidos e 228 não atualizados.\n
É preciso obter 0 B/935 kB de arquivos. Depois do desempacotamento, 2.225 kB serão usados.\n
Pré-configurando pacotes ...                     \n 
A seleccionar pacote anteriormente não seleccionado isc-dhcp-server.\n
(Lendo banco de dados ... 150708 ficheiros e directórios actualmente instalados.)\n
Desempacotando isc-dhcp-server (de .../isc-dhcp-server_4.2.2.dfsg.1-5+deb70u8_i386.deb) ...\n
Processando gatilhos para man-db ...\n
Configurando isc-dhcp-server (4.2.2.dfsg.1-5+deb70u8) ...\n
Generating /etc/default/isc-dhcp-server...\n
\n
[FAIL] Starting ISC DHCP server: dhcpd[....] check syslog for diagnostics. ... failed!\n
 failed!\n
\n
invoke-rc.d: initscript isc-dhcp-server, action \"start\" failed.\n
\n
"

"
PASSO 5$RED(comentado)$END: INSTALAR O SERVIDOR DHCP:\n
Agora podemos instalar o servidor através do comando:\n
# aptitude install isc-dhcp-server\n
Os NOVOS pacotes a seguir serão instalados:\n
  isc-dhcp-server \n
0 pacotes atualizados, 1 novos instalados, 0 a serem removidos e 228 não atualizados.\n
É preciso obter 0 B/935 kB de arquivos. Depois do desempacotamento, 2.225 kB serão usados.\n
Pré-configurando pacotes ...                     \n 
A seleccionar pacote anteriormente não seleccionado isc-dhcp-server.\n
(Lendo banco de dados ... 150708 ficheiros e directórios actualmente instalados.)\n
Desempacotando isc-dhcp-server (de .../isc-dhcp-server_4.2.2.dfsg.1-5+deb70u8_i386.deb) ...\n
Processando gatilhos para man-db ...\n
Configurando isc-dhcp-server (4.2.2.dfsg.1-5+deb70u8) ...\n
Generating /etc/default/isc-dhcp-server...\n
$RED(o instalador de pacote gera uma configuração padrão)$END\n
[FAIL] Starting ISC DHCP server: dhcpd[....] check syslog for diagnostics. ... failed!\n
 failed!\n
$RED(o instalador tenta iniciar o servidor)$END\n
invoke-rc.d: initscript isc-dhcp-server, action \"start\" failed.\n
$RED(ignore as mensagens de erro, neste caso são normais)$END\n
"

"
PASSO 6: VERIFICAR OS 'ERROS' DA INSTALAÇÃO PADRÃO:\n
Para verificar as mensagens da inicialização do servidor DHCP, execute o comando:\n
# tail -20 /var/log/syslog\n
\n
...\n
Jun 26 19:58:27 test-host dhcpd: No subnet declaration for eth1 (10.200.0.1).\n
Jun 26 19:58:27 test-host dhcpd: ** Ignoring requests on eth1.  If this is not what\n
Jun 26 19:58:27 test-host dhcpd:    you want, please write a subnet declaration\n
Jun 26 19:58:27 test-host dhcpd:    in your dhcpd.conf file for the network segment\n
Jun 26 19:58:27 test-host dhcpd:    to which interface eth1 is attached. **\n
...\n
Jun 26 19:58:27 test-host dhcpd: Not configured to listen on any interfaces!\n
\n
"

"
PASSO 6$RED(comentado)$END: VERIFICAR OS 'ERROS' DA INSTALAÇÃO PADRÃO:\n
Para verificar as mensagens da inicialização do servidor DHCP, execute o comando:\n
# tail -20 /var/log/syslog\n
$RED(lista as 20 últimas linhas do arquivo de log)$END\n
...\n
Jun 26 19:58:27 test-host dhcpd: No subnet declaration for eth1 (10.200.0.1).\n
Jun 26 19:58:27 test-host dhcpd: ** Ignoring requests on eth1.  If this is not what\n
Jun 26 19:58:27 test-host dhcpd:    you want, please write a subnet declaration\n
Jun 26 19:58:27 test-host dhcpd:    in your dhcpd.conf file for the network segment\n
Jun 26 19:58:27 test-host dhcpd:    to which interface eth1 is attached. **\n
...\n
Jun 26 19:58:27 test-host dhcpd: Not configured to listen on any interfaces!\n
$RED(o 'erro' é na verdade um 'alerta' informou que o DHCP não está ativo, pois não está plenamente configurado)$END\n
"

"
INSTRUÇÃO 2: CONFIGURAÇÃO DO SERVIDOR DHCP - CONCEITOS BÁSICOS\n
- A configuração do servidor DHCP se localiza em /etc/dhcp/dhcpd.conf\n
  - os itens que se iniciam com 'option', referem-se a configuração para o cliente;
  - caso contrário, se referem a configuração do próprio servidor;
- O arquivo se divide dois tipos de opções:\n
  - globais, para todas as sub-redes servidas;\n
  - específicas, para cada sub-rede servida;\n
- As opções globais essencias são essas: \n
  - option domain-name = nome do domínio utilizado pelo cliente para se auto-identificar.\n
  - Para verificar o valor atual do nome do domínio, execute 'hostname -f'\n
  - option domain-name-servers = lista dos números IP dos servidores DNS\n
  - default-lease-time = tempo de empréstimo padrão\n
  - max-lease-time = tempo de empréstimo máximo\n
- As opções específicas por sub-rede são essas:\n
  - range = faixa de números IPs a serem emprestados\n
  - option routers = define o roteador padrão\n
- Para saber mais, execute o comando:\n
$ man dhcp-options\n
"

"
PASSO 7: ALTERAR O ARQUIVO DE CONFIGURAÇÃO DO SERVIDOR DHCP\n
- Considere como exemplo que temos uma rede interna entre o servidor e o cliente\n
  - sub-rede: 10.200.0.0\n
  - domínio: inf.ufg.br\n
  - servidor DNS: 8.8.8.8\n
  - máscara: 255.255.0.0\n
  - número IP do gateway: 10.200.0.1\n
  - tempo de empréstimo padrão: 24 horas (1.440 s)\n
    (quanto maior esse tempo, a configuração vai durar mais tempo)\n
    (no entanto, caso tenham se muitas requisições por dia, a faixa de IPs pode exaurir)\n
  - tempo de empréstimo máximo: 3 dias (4.320 s)\n
  - faixa de números IP para clientes:\n
    - 10.200.0.10 a 10.200.0.200\n
- Para aplicar as configurações acima, edite:\n
# nano /etc/dhcp/dhcpd.conf\n
...\n
option domain-name \"inf.ufg.br\";\n
default-lease-time 1440;\n
max-lease-time 4320;\n
subnet 10.200.0.0 netmask 255.255.0.0 {\n
   range 10.200.0.10 10.200.0.200;\n
   option routers 10.200.0.1;\n
   option domain-name-servers 8.8.8.8;\n
}\n
...\n
"

"
PASSO 8: INICIAR E CHECAR O SERVIDOR DHCP\n
- Execute o comando em um terminal como root:\n
# service isc-dhcp-server start\n
[ ok ] Starting ISC DHCP server: dhcpd.\n
\n

- Para checar o resultado da inicialização, execute:\n
# tail /var/log/syslog\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd: No subnet declaration for eth0 (192.168.0.41).\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd: ** Ignoring requests on eth0. \n
                                       If this is not what\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd:    you want, please write a subnet declaration\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd:    in your dhcpd.conf file for the network segment\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd:    to which interface eth0 is attached. **\n
\n
\n
\n
"


"
PASSO 8$RED(comentado)$END: INICIAR E CHECAR O SERVIDOR DHCP\n
- Execute o comando em um terminal como root:\n
# service isc-dhcp-server start\n
[ ok ] Starting ISC DHCP server: dhcpd.\n
$RED(o resultado foi OK dessa vez)$END\n

- Para checar o resultado da inicialização, execute:\n
# tail /var/log/syslog\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd: No subnet declaration for eth0 (192.168.0.41).\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd: ** Ignoring requests on eth0. \n
                                       If this is not what\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd:    you want, please write a subnet declaration\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd:    in your dhcpd.conf file for the network segment\n
Jun 27 01:59:31 debian-adsi-2014 dhcpd:    to which interface eth0 is attached. **\n
$RED(note que ocorreu somente uma advertência)$END\n
$RED(o servidor DHCP não pode atuar na interface eth0)$END\n
$RED(na interface eth0 já atua um servidor DHCP da rede local)$END\n
"

"
PASSO 9: ENTRAR NA MV2\n
- Considere que já tenhamos conectividade entre o servidor (MV1) e cliente (MV2)\n
  - IP estático da MV2: 10.200.0.2\n
- Para testar, execute o comando ping da MV1 para MV2:\n
$  ping -c 1 10.200.0.2\n
PING 10.200.0.2 (10.200.0.2) 56(84) bytes of data.\n
64 bytes from 10.200.0.2: icmp_req=1 ttl=64 time=0.282 ms\n
\n
--- 10.200.0.2 ping statistics ---\n
1 packets transmitted, 1 received, 0% packet loss, time 0ms\n
rtt min/avg/max/mdev = 0.282/0.282/0.282/0.000 ms\n
\n
- para entrar no cliente MV2, execute o comando ssh:\n
$ ssh teste@10.200.0.2\n
...\n
teste@debian-minimo:~$\n
\n
- para poder editar a configuração de rede da MV2, é necessário poderes de root:\n
teste@debian-minimo:~$ su -\n
root@debian-minimo:~#\n
\n
"

"
PASSO 9$RED(comentado)$END: ENTRAR NA MV2\n
- Considere que já tenhamos conectividade entre o servidor (MV1) e cliente (MV2)\n
  - IP estático da MV2: 10.200.0.2\n
- Para testar, execute o comando ping da MV1 para MV2:\n
$  ping -c 1 10.200.0.2\n
PING 10.200.0.2 (10.200.0.2) 56(84) bytes of data.\n
64 bytes from 10.200.0.2: icmp_req=1 ttl=64 time=0.282 ms\n
\n
--- 10.200.0.2 ping statistics ---\n
1 packets transmitted, 1 received, 0% packet loss, time 0ms\n
rtt min/avg/max/mdev = 0.282/0.282/0.282/0.000 ms\n
$RED(não houve perda de pacotes, a conectividade está OK)$END\n
- para entrar no cliente MV2, execute o comando ssh:\n
$ ssh teste@10.200.0.2\n
...\n
teste@debian-minimo:~$\n
$RED(se o login foi bem sucedido (senha: 123456), entrou-se na MV2)$END\n
- para poder editar a configuração de rede da MV2, é necessário poderes de root:\n
teste@debian-minimo:~$ su -\n
root@debian-minimo:~#\n
$READ(o prompt com '#' indica poderes de super-usuário)$END\n
"

"
PASSO 10: ACRESCENTAR A CONFIGURAÇÃO DE REDE DINÂMICA PARA ETH1:\n
- Para se testar o cliente DHCP, é necessário alterar a configuração de rede\n
- Na máquina MV2, edite o arquivo de configuração de rede:\n
# nano /etc/network/interfaces\n
...\n
iface eth1 inet dhcp\n
\n
...\n
"

"
PASSO 10$RED(comentado)$END: ALTERAR A CONFIGURAÇÃO DE REDE DE ESTÁTICA PARA DINÂMICA:\n
- Para se testar o cliente DHCP, é necessário alterar a configuração de rede\n
- Na máquina MV2, edite o arquivo de configuração de rede:\n
# nano /etc/network/interfaces\n
...\n
iface eth1 inet dhcp\n
$RED(note que basta a linha acima, para se obter configuração dinâmica)$END\n
...\n
"

"
PASSO 11: REINICIAR A INTERFACE DE REDE ETH1 E TESTAR:\n
- Para que a configuração do arquivo seja recarregada, é necessário reiniciar a interface de rede\n
- Os seguintes comandos reiniciam a interface de rede:\n
# ifdown eth1\n
# ifup eth1\n
DHCPDISCOVER on eth1 to 255.255.255.255 port 67 interval 8\n
DHCPREQUEST on eth1 to 255.255.255.255 port 67\n
DHCPOFFER from 10.200.0.1\n
DHCPACK from 10.200.0.1\n
bound to 10.200.0.11 -- renewal in 578 seconds.\n
\n
\n
\n
"

"
PASSO 11$RED(comentado)$END: REINICIAR A INTERFACE DE REDE ETH1 E TESTAR:\n
- Para que a configuração do arquivo seja recarregada, é necessário reiniciar a interface de rede\n
- Os seguintes comandos reiniciam a interface de rede:\n
# ifdown eth1\n
# ifup eth1\n
DHCPDISCOVER on eth1 to 255.255.255.255 port 67 interval 8\n
DHCPREQUEST on eth1 to 255.255.255.255 port 67\n
DHCPOFFER from 10.200.0.1\n
DHCPACK from 10.200.0.1\n
bound to 10.200.0.11 -- renewal in 578 seconds.\n
$RED(o servidor DHCP 10.200.0.1 respondeu o pedido)$END\n
$RED(o número IP fornecido foi 10.200.0.1)$END\n
$RED(poderia se utilizar somente uma interface eth0, mas esta estava sendo utilizada pela conexão ssh, então por isso se optou por testar em outra interface (eth1) )$END\n
"

"
FIM DO TUTORIAL: BONS ESTUDOS!\n
- Leia mais sobre o DHCP:\n
  - https://pt.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol\n
  $RED(Leia sobre os pacotes UDP DHCPDISCOVER, DHCPREQUEST, DHCPOFFER e DHCPACK)$END\n
"
)
